Title: Development

* [Development Setup](https://biglife.quest/~tildes.official/wiki/development/initial_setup): the requirements and steps to set up your own local development version of Tildes.

* [Development](https://biglife.quest/~tildes.official/wiki/development/general_development): how to work on your local development version of Tildes.
