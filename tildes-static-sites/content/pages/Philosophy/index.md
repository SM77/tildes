Title: Philosophy

These pages cover the main philosophies behind Tildes: why it exists, how it will operate, and what drives the decisions behind its implementation.

The ["Announcing Tildes" blog post](https://blog.biglife.quest/announcing-tildes) also covers some of this material.

* [Philosophy: People](https://biglife.quest/~tildes.official/wiki/philosophy/people)

* [Philosophy: Content](https://biglife.quest/~tildes.official/wiki/philosophy/content)

* [Philosophy: Privacy](https://biglife.quest/~tildes.official/wiki/philosophy/privacy)

* [Philosophy: Site design](https://biglife.quest/~tildes.official/wiki/philosophy/site_design)

* [Philosophy: Site implementation](https://biglife.quest/~tildes.official/wiki/philosophy/site_implementation)
