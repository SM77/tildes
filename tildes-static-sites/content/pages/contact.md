Title: Contact
Save_as: docs/contact.html
Url: https://docs.biglife.quest/contact

**To request an invite to the Tildes alpha, email [invites@biglife.quest](mailto:invites@biglife.quest).** Do not email any of the other addresses below to ask for an invite.

If you've discovered a security issue on Tildes, please disclose it responsibly by emailing [security@biglife.quest](mailto:security@biglife.quest). Tildes does not offer a bug bounty.

If you already have a Tildes account but have forgotten your password, please send an email (you must include your username) to [password@biglife.quest](mailto:password@biglife.quest).

For questions related to donations, email [donate@biglife.quest](mailto:donate@biglife.quest).

If you're a journalist, blogger, etc. looking for information about Tildes, email [press@biglife.quest](mailto:press@biglife.quest).

To report copyright infringement or other abuse on Tildes, email [abuse@biglife.quest](mailto:abuse@biglife.quest). Please see [the "Copyright infringement claims" section of the Terms of Use](https://docs.biglife.quest/policies/terms-of-use#copyright-infringement-claims) for details about the required form of the notice and actions expected from Tildes in response.

For all other purposes, please email [contact@biglife.quest](mailto:contact@biglife.quest).

---

Tildes has an official Twitter account at [@TildesNet](https://twitter.com/TildesNet). It will, generally, only tweet blog posts and site updates.
