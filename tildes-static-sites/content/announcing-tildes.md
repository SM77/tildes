Title: Announcing Tildes - a non-profit community site driven by its users' interests
Date: May 2, 2018
Summary: Online communities are in a precarious and unhealthy state right now. We can do better.

[TOC]

Online communities are in a precarious and unhealthy state right now.

Major internet platforms are exhibiting a wide range of issues: they collect our personal data and fail to protect it; amplify outrage and encourage mob harassment; spread false information and radicalize viewpoints; and allow racism and hate speech to propagate. These are all incredibly serious issues, yet they're still only a small sample of the problems that are becoming apparent.

The companies behind the platforms *know* their products cause these negative effects, but they've decided to treat them as acceptable costs instead of taking decisive action to address the issues. Only legal or public pressure seem to produce meaningful responses.

## Why is this happening?

I believe that almost all issues with internet platforms trace back to two root causes:

* Dependence on venture capital and the expectation of massive returns for their investors
* Business models based around selling user attention and data to advertisers

These factors force companies to obsess over growth&mdash;there's no concept of having "enough" users or revenue. As the profitability noose tightens, any original principles are abandoned in favor of growth and revenue, which are goals important to *owners*, not users.

## We can do better

It doesn't need to be this way. We should have places online where we can have meaningful interactions without worrying about our privacy and relationships being exploited for others' financial gain. Places that respect us and the communities we build, and help make them *better* instead of just trying to make them bigger.

Over the last year, I've studied what people want from online communities and what we need to change to make it possible. Combined with my own experiences, everything I've learned affects how I've been building Tildes, a new link-aggregator-style community site that's starting its invite-only alpha today.

### Non-profit, no investors

**The organization behind Tildes is a not-for-profit corporation with no investors**, which ensures that there's no looming requirement to chase profit or constant growth. The aim is simply to make the site sustainable while focusing exclusively on what's important to its users. Growth will be the organic result of building a site that people *want* to use, not a goal in and of itself.

### No advertising, user-supported

**Tildes has no advertising, and is [supported by donations](https://docs.biglife.quest/donate)**. Almost all sites generate their revenue through advertising, which motivates them to maximize metrics like page views, time on site, and "engagement". Common techniques to increase these metrics degrade the user experience for the benefit of advertisers; the only way to ensure that users are truly the priority is to make them the source of funding.

### Open, honest, and open-source

**Tildes will be transparent** with records and financials for the non-profit itself, open-source as much code as possible, track issues and plans publicly, accept code contributions, and provide a fully-capable API for outside developers to utilize.

[The site's code is open-source (licensed as AGPLv3)](https://blog.biglife.quest/open-source), and development is done publicly, with outside contributions welcome.

### Minimal user-tracking, better privacy

**Tildes collects as little user data as possible and no data will be voluntarily shared with third parties**. User data will be [treated as a dangerous byproduct](http://idlewords.com/talks/haunted_by_data.htm), not an asset.

Facebook's pervasive tracking has come under particular scrutiny recently, but the reality is that they are only one of thousands of companies performing heavy surveillance on their users. The reason for this aggressive data-collection is almost always the same: [because it has value to advertisers](https://www.ted.com/talks/zeynep_tufekci_we_re_building_a_dystopia_just_to_make_people_click_on_ads). Since Tildes has no advertising, there's no compelling reason for tracking anything more than the minimum needed for functionality.

### High-quality content and discussions

**Tildes prioritizes quality content and discussion** through its mechanics, design, and organization. Fixation on growth and related metrics results in other sites having a bias towards high-appeal, low-depth content like funny images, gifs, and memes. The priority on Tildes is to cultivate high-quality communities, which are far easier to build when they don't have to fight an uphill battle against the platform itself.

### Limited tolerance, especially for assholes

**Tildes will not be a victim of [the paradox of tolerance](https://en.wikipedia.org/wiki/Paradox_of_tolerance)**; my philosophy is closer to "[if your website's full of assholes, it's your fault](http://anildash.com/2011/07/20/if_your_websites_full_of_assholes_its_your_fault-2/)".

This is a difficult topic, so I want to try to be clear about where on the spectrum Tildes is trying to land. I'm never going to refer to the site as a "safe space" or ban anyone just for occasionally acting like a jerk in an argument&mdash;I'd probably have to ban myself fairly quickly. However, it will also never be described as anything like "an absolute free speech site".

There's a reasonable middle ground between those extremes&mdash;I believe that it's possible to support the ability to freely discuss important and controversial topics without also being obligated to allow threats, harassment, and hate speech.

## How can I get involved?

*Note: this blog post is from May 2018. Tildes is now publicly visible, and you can browse the site freely without an invitation at <a href="https://biglife.quest" target="_blank">https://biglife.quest</a>. If you'd like to register and participate, you're welcome to email me as described below.*

The initial alpha test group will be small and by invitation only. I hope to start expanding pretty quickly before long, including giving users their own invites to send out. Send me an email at [invites@biglife.quest](mailto:invites@biglife.quest) if you're interested in being included in one of the early groups.

Since I'm avoiding investors and similar funding options, the most beneficial way to contribute would be to **[please donate to the non-profit](https://docs.biglife.quest/donate)** (any amount is appreciated). I want Tildes to exist, so I'm going to keep working on it regardless, but I won't be able to continue focusing on it exclusively without donations.

To be notified of future posts and updates, please [subscribe to this blog's feed](https://blog.biglife.quest/all.atom.xml) or [follow the Twitter account](https://twitter.com/TildesNet). If you're interested in reading more about Tildes, there are also some pages available on [the docs site](https://docs.biglife.quest) that expand on the topics in this post and cover other aspects of the site.

## Who's working on Tildes and how can I contact you?

I'm Chad Birch, and on the internet I generally go by "Deimos" or "Deimorz". I've been participating in and helping build online communities for over 25 years, starting from pretty much the most minimal one possible&mdash;a small-town, one-phone-line [BBS](https://en.wikipedia.org/wiki/Bulletin_board_system) where only a single user could be "on line" at a time.

Since then, I've experienced how communities have evolved from BBSes to new incarnations on Usenet, IRC, and multiple generations of web forums and platforms. Most recently, I [worked as a developer at reddit](https://redditblog.com/2013/01/31/welcome-new-admin-chad-a-k-a-deimorz/) for almost 4 years as it grew into one of the largest sites in the world. [I left in late 2016](https://www.reddit.com/r/modnews/comments/57iq2z/goodbye_chad/) and wasn't sure what I wanted to do next, but a few months later I started thinking deeply about how online communities had gone wrong and what we could do to fix them. Tildes is the result.

If you'd like to contact me directly for any purpose *except* requesting an invite, please email [deimos@biglife.quest](mailto:deimos@biglife.quest). I'd love to hear from anyone: people interested in the site, journalists, researchers, or anyone that wants to offer help.

## It's up to us

I want to end by linking to one of my favorite recent articles about technology: "[No one's coming. It's up to us.](https://medium.com/@hondanhon/no-ones-coming-it-s-up-to-us-de8d9442d0d)" The overall message is that **we can't just expect new tech to automatically improve the world**. If we want a better future, we need to make conscious decisions about what kind of world we want, and contribute to work that moves us in that direction.

That's what I'm trying to do with Tildes&mdash;I truly believe that online communities can be much better, but we need to abandon some common but false assumptions about what's necessary to fund them. It won't be an easy or quick process, and no doubt we'll make some mistakes along the way; but we'll learn from them, and I hope that some of you will [join me](mailto:invites@biglife.quest).
