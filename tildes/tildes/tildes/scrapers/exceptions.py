# Copyright (c) 2019 Tildes contributors <code@biglife.quest>
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Exception classes related to scraping."""


class ScraperError(Exception):
    """Exception class for an error while scraping."""

    pass
